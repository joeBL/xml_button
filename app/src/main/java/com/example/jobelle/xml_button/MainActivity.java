package com.example.jobelle.xml_button;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.relative_layout);

        final Button button1 = (Button) findViewById(R.id.redButton);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView red = (TextView) findViewById(R.id.red);
                TextView yellow = (TextView) findViewById(R.id.yellow);
                TextView blue = (TextView) findViewById(R.id.blue);
                TextView purple = (TextView) findViewById(R.id.purple);
                TextView green = (TextView) findViewById(R.id.green);
                red.setBackgroundColor(Color.RED);
                yellow.setBackgroundColor(Color.RED);
                blue.setBackgroundColor(Color.RED);
                purple.setBackgroundColor(Color.RED);
                green.setBackgroundColor(Color.RED);
            }
        });

        final Button button2 = (Button) findViewById(R.id.blueButton);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView red = (TextView) findViewById(R.id.red);
                TextView yellow = (TextView) findViewById(R.id.yellow);
                TextView blue = (TextView) findViewById(R.id.blue);
                TextView purple = (TextView) findViewById(R.id.purple);
                TextView green = (TextView) findViewById(R.id.green);
                red.setBackgroundColor(Color.BLUE);
                yellow.setBackgroundColor(Color.BLUE);
                blue.setBackgroundColor(Color.BLUE);
                purple.setBackgroundColor(Color.BLUE);
                green.setBackgroundColor(Color.BLUE);
            }
        });

        final Button button3 = (Button) findViewById(R.id.resetButton);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView red = (TextView) findViewById(R.id.red);
                TextView yellow = (TextView) findViewById(R.id.yellow);
                TextView blue = (TextView) findViewById(R.id.blue);
                TextView purple = (TextView) findViewById(R.id.purple);
                TextView green = (TextView) findViewById(R.id.green);
                red.setBackgroundColor(Color.parseColor("#F44336"));
                yellow.setBackgroundColor(Color.parseColor("#FFEB3B"));
                blue.setBackgroundColor(Color.parseColor("#0D47A1"));
                purple.setBackgroundColor(Color.parseColor("#6200EA"));
                green.setBackgroundColor(Color.parseColor("#76FF03"));
            }
        });

    }

//    public void setToRed(View view) {
//        TextView red = (TextView) findViewById(R.id.red);
//        TextView yellow = (TextView) findViewById(R.id.yellow);
//        TextView blue = (TextView) findViewById(R.id.blue);
//        TextView purple = (TextView) findViewById(R.id.purple);
//        TextView green = (TextView) findViewById(R.id.green);
//        red.setBackgroundColor(Color.RED);
//        yellow.setBackgroundColor(Color.RED);
//        blue.setBackgroundColor(Color.RED);
//        purple.setBackgroundColor(Color.RED);
//        green.setBackgroundColor(Color.RED);
//    }
//
//    public void setToBlue(View view) {
//        TextView red = (TextView) findViewById(R.id.red);
//        TextView yellow = (TextView) findViewById(R.id.yellow);
//        TextView blue = (TextView) findViewById(R.id.blue);
//        TextView purple = (TextView) findViewById(R.id.purple);
//        TextView green = (TextView) findViewById(R.id.green);
//        red.setBackgroundColor(Color.BLUE);
//        yellow.setBackgroundColor(Color.BLUE);
//        blue.setBackgroundColor(Color.BLUE);
//        purple.setBackgroundColor(Color.BLUE);
//        green.setBackgroundColor(Color.BLUE);
//    }
//
//    public void resetColor(View view){
//        TextView red = (TextView) findViewById(R.id.red);
//        TextView yellow = (TextView) findViewById(R.id.yellow);
//        TextView blue = (TextView) findViewById(R.id.blue);
//        TextView purple = (TextView) findViewById(R.id.purple);
//        TextView green = (TextView) findViewById(R.id.green);
//        red.setBackgroundColor(Color.parseColor("#F44336"));
//        yellow.setBackgroundColor(Color.parseColor("#FFEB3B"));
//        blue.setBackgroundColor(Color.parseColor("#0D47A1"));
//        purple.setBackgroundColor(Color.parseColor("#6200EA"));
//        green.setBackgroundColor(Color.parseColor("#76FF03"));
//    }
}

